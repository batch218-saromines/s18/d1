// console.log("hello")


// function printInfo(){
// 	let nickName = prompt ("Enter your nickname: ");
// 	console.log("Hi " + nickName);

// }
// printInfo(); //invoke

//-----------------------------------
// parameter

function printName(name){
	console.log("My name is " + name);
}

printName("Juana");//argument
printName("John");
printName("Cena");


// "firstName" is called a parameter
// it acts as a named variable/container that exists only inside a functiion

let sampleVariable = "Inday";

printName(sampleVariable)


// Variable can alse be passed as an argument

// ----------------------------------------------

function checkDivisibilityBy8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


// ------------

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){;
	argumentFunction();
	}

	invokeFunction(argumentFunction)
	console.log(argumentFunction)

	// -----------------------

	function createFullName(firstName, middleName, lastName){
	console.log("My full name is " +  firstName + " " + middleName + " " + lastName);
		}
	createFullName ("Hannah", "Mae", "Saromines");

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);


	// --------------------

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference of 8 and 4 " + (numA - numB));
	}

	getDifferenceOf8Minus4(8,4);

	console.log("getDifferenceOf8Minus4")


	// Return Statement

	function returnFullName(firstName, middleName, lastName){

		return firstName + " " + middleName + " " + lastName;


		// this line will not be printed
		console.log("This is printed inside a function")
	}

	let completeName = returnFullName("1", "2", "3");
	console.log(completeName);

	console.log("I am " + completeName);


	function printPlayerInfo(userName,level,job){

	console.log ("Username: " + userName )
	console.log ("Level: " + level )
	console.log ("Job: " + job )

	return {userName, level, job}

}
	let user1 = printPlayerInfo("aa","bb","cc");
	console.log(user1);
